Make
====

**make** (englisch für machen, erstellen) ist ein Build-Management-Tool, das Kommandos in Abhängigkeit von Bedingungen ausführt. 

Das Erstellen einer Datei wird im Makefile als ein Ziel (Target) bezeichnet. Die Randbedingungen dazu werden in einem Eintrag beschrieben

     A: B C
       cmd_A

**Beispiel**:

    Test.txt:
            touch Test.txt
            
**Aufruf**:

Make kann im Terminal von Eclipse Theia aufgerufen werden.

    cdi/apps/04-make
    make

                
